/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';


import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  Image,
  DrawerLayoutAndroid,
  TouchableOpacity,
  UIManager,
  findNodeHandle,
  YellowBox,
  FlatList,
} from 'react-native';


const ICON_SIZE=24

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import { ButtonGroup } from 'react-native-elements';

class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      items: [{
        postname: "Nithya" ,  
        postdesc: "It was wonderful to spend time with you",
        uri: require("./images/nithya.png")
      },
      {
        postname: "Instagram",
        postdesc:"It was awesome to stay here",
        uri: require("./images/nithya2.png")
      },
      {
        postname: "Facebook",
        postdesc:"on a good day",
        uri: require("./images/nithya3.png")
      },
      {
        postname: "Keep smiling",
        postdesc: "Because smiling is good for health:)",
        uri: require("./images/nithya8.png")
      }
       ]
    }
  }
  listItems() {
    let items = this.state.items;
    let day = new Date().getDate(); //Current Date
    let month = new Date().getMonth() + 1; //Current Month
    let year = new Date().getFullYear();//Current Year
    let hours = new Date().getHours();
    let mins = new Date().getMinutes();
    let secs = new Date().getSeconds();
    return (
         
        
          items.map((val) => {
            return (
  <View>
    <View style={styles.newcontainer}>
      <Image source={require('./images/nithya.png')} style = {{height: 50, width: 50, resizeMode : 'contain',}} />
      <Text style={styles.text1}>Nithya shree {"\n"}  
      <Text style={{fontSize: 10, color: 'grey'}}>{day}:{month}:{year} at {hours}:{mins}:{secs}</Text></Text>
      
      <MenuProvider>
   <Menu onSelect={value => alert(`You Clicked : ${value}`)}>
     <MenuTrigger >
     <Image source={require('./images/download.png')} style = {{height: 50, width: 50,justifyContent: 'flex-end', resizeMode : 'stretch',}} />
     </MenuTrigger >
     <MenuOptions>
       <MenuOption value={"Edit"}>
         <Text>Edit</Text>
       </MenuOption>
       <MenuOption value={"Remove"}>
         <Text>Remove</Text>
       </MenuOption>
       <MenuOption value={"Share"}>
         <Text>Share</Text>
       </MenuOption>
     </MenuOptions>
   </Menu>
 </MenuProvider> 
      

    </View> 
         <Text >{val.postname}</Text>
         <Text >{val.postdesc}</Text>
        <Image source={val.uri} style = {{height: 250, width: '100%',flexDirection: 'row',flexWrap: 'wrap', resizeMode : 'center',}} />
        </View>      
              
            );
          })
    );
  }

  

  
  render()
  {
    
    
    
      return (
       
        
    <View style={styles.MainContainer}>
     

      <ScrollView style ={styles.scrollView}>
         
        { this.listItems() }
       
        </ScrollView>
        </View>
        
       
      );
      }
     

    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
       
      },
      scrollView: {
        backgroundColor: 'white',
        marginHorizontal: 10,
      },
      text1: {
        fontWeight: 'bold',
        color: 'blue',
        fontSize: 20,
        fontFamily: 'Roboto',

      },
      text: {
        fontFamily: 'Roboto',
        fontSize: 15,
        
        
      },
      MainContainer: 
     {
 
    flex: 6,
    flexDirection: 'row',
 
// Set content's vertical alignment.
    justifyContent: 'center',
 
// Set content's horizontal alignment.
   alignItems: 'center',
 
// Set hex color code here.
   backgroundColor: 'blue',
 
},
separator: {
  borderBottomColor: '#bbb',
  borderBottomWidth: StyleSheet.hairlineWidth,
},
newcontainer: {
  flex: 1,
  alignContent: 'flex-start',
  flexDirection: 'row',
},
title: {
  fontWeight: 'bold',
  fontSize: 15,
  color: 'black',

},
rightButton: {
  width: 100,
  height: 37,
  position: 'absolute',
  bottom: 8,
  right: 2,
  padding: 8
},

    });
      
     


export default App;
